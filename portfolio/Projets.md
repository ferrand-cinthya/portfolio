---
hide:
  - toc
  - site_nav
---

### Detection de masque (Covid)

![Image](./static/git_web.gif){: style="width: 40%" } 

> groupe 3 personnes en 3 semaines

Réalisation d'une application permettent à l'entreprise Simplon.co de verrifier les port du masque de ses étudiants.

les objectifs étaient de :
- Créer et traiter un jeux de donnée d'images
- Créer et optimser un algortmes de detection 
- Monitorer la perfomance de l'application 

### Prediction du Nutriscore

![Image](./static/nutri.jpg){: style="width: 40%" } 

> groupe 2 personnes en 2 semaines

Sur la base des donnée de Open Food Facts le but était de remplir les notes nutriscores non remplis par les consommateurs. 

les objectifs étaient de :
- Créer un algorithme dummy pour voir la performence du model entrainé.
- Créer et optimser un algortmes complexe
- Intégrer le model dans une application  

### Projet client Teklia 

![Image](./static/projet.jpg){: style="width: 40%" } 

> projet individuel

Réalisation d'un programme permettent l'étude d'un rapport d'étude sur une intelligence artificielle qui transcrit les manuscrits en donnée numérique.

les objectifs étaient de :
- Parse les données
- Les intégrés dans une base de donnée SQLite
- Créer un rapport permettent de ressortir les données intéressante


### [dashboard job](https://gitlab.com/simplon-dev-data/grenoble-2020-2021/projets/projet_3/projet_3-groupe_2/-/tree/projet_3_master)

> groupe 5 personnes en 3 semaines avec la méthodologie SCRUM

La création d'un site réunissant les offres d'emploi de la data de plusieurs sites mettant en évidence des informations permettant l'étude des futures demandes dans le domaine.
Les objectifs étaient de :

- Créer une table d'offres d'emploi "Data" dans la région mise à jour toutes les 24 h.
- Visualiser les influences des métiers par titre.
- Mettre valeur les annonces en fonction de critères mots-clés.

https://simplon-dev-data.gitlab.io/grenoble-2020-2021/projets/projet_3/projet_3-groupe_2/

_Compétences_ : collecte, traitement et visualisation des données, mise à disposition sur un site.

_Technologies_ : Python, Gitlab, Jupyter notebook

![Tableau Job](./static/graph.png){: style="width: 20%" }
![Tableau Job](./static/carte.png){: style="width: 20%" }

### [Pollution de l air en Rhône-Alpes](https://gitlab.com/ferrand.cinthya/pollution-de-l-air-en-rhone-alpe/)

> groupe 3 personnes (chef de groupe) en 2 semaines

Le but était d'étudier via les données Atom Auvergne Rhône-Alpes. (https://data-atmoaura.opendata.arcgis.com/)
Les objectifs étaient de :

- Visualiser une carte avec les différentes balises.
- Créer des graphiques montrant l'évolution des polluants toutes les heures.
- Démontrer l'impact du covid sur la qualité de l'air.

_Compétences_ : Concevoir et structurer physiquement une base de données, créer des représentations graphiques, gérer un planning d'équipe.

_Technologies_ : Git lab, Gantt, Python, SQLite, Metabase

![étude de la pollution](./static/pollu.png){: style="width: 30%" }
