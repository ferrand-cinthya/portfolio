---
hide:
  - toc
  - site_nav
---

![Image](./static/formation.png){: style="width: 40%" } 

### [Simplon Développeur.se Data/ IA intelligence artificielle ](https://simplon.co)
> Novembre 2021 - juin 2023, Grenoble 

Suite de la formation précedente le développeur en IA est un spécialiste du développement d’applicatifs informatiques autour de l’IA et de la Data Science. Intégré dans la résolution d’une problématique métier définie par l’organisation, son rôle est de développer des solutions informatiques utilisables par des spécialistes et des non-spécialistes, intégrant directement ou indirectement des briques d’Intelligence Artificielle (par exemple : algorithmes de Machine Learning). Il conçoit, teste et adapte les applicatifs intégrant tout ou partie de ces technologies.

Il est donc spécialiste du développement informatique, du génie logiciel et des interfaces Hommes-Machines, avec une très bonne connaissance des technologies d’IA/Data Science, du secteur ou de la fonction d’application des données traitées. 



### [Simplon Développeur.se Data](https://simplon.co)

> Octobre 2020 - Septembre 2021, Grenoble

De l’analyse du besoin à la data visualisation, en passant par la collecte et le traitement des données, le développeur data conçoit et exploite les bases de données.

Le développeur data gère l'ensemble du cycle de vie de la donnée, de la donnée brute jusqu'à la livraison de données utilisables. Il s’agit d'un technicien capable d'appréhender n'importe quel type de format de données, de les stocker en base de données, les interroger et de les servir, avec un rendu visuel ou un support adapté pour un usage tiers. Il peut être amené à automatiser des processus d'acquisition, d'import, d'extraction et de visualisation de données. Il est le garant de la qualité, de l'intégrité et de la cohérence des données avant et après traitement.

Le métier de développeur data s’articule alors autour de 2 activités principales :

- Développer une base de données
- Exploiter une base de données

La méthodologie d'apprentissage Simplon est basée sur la pédagogie active, des travaux de groupe avec des personnes d'horizons et d'experiences divers avec des cas d'applications réels d'entreprises.

### Bac Professionnel Services aux personnes et aux territoires

> 2014-2016

Cette formation permet d'apprendre les rudiments de l'entretien, de la cuisine, de l'informatique et de l'animation afin de gérer l'ensemble des soins à la personne (bébé, personne âgée, personne en situation de handicap) et aux tâches administratives.

Tâches principales :
- entretien des locaux

- soins aux personnes (aide à la toilette, aux repas, ...)

- animation

- cuisine

- utilisation des outils informatique (Word, Excel, Power Point)

## [Mon CV](./static/CV.pdf)
