---
hide:
  - toc
  - site_nav
---

![Image](./static/tete.jpeg){: style="width: 20%" }
# FERRAND Cinthya

Développeuse Data / IA Junior titre RNCP bac+3/4

---

[:fontawesome-brands-linkedin:{: style="transform: scale(1.5); margin: 0 0.5rem;" }]( https://www.linkedin.com/in/cinthya-ferrand-962609207/ "LinkedIn")
[:fontawesome-brands-gitlab:{: style="transform: scale(1.5); margin: 0 0.5rem;" }](https://gitlab.com/users/ferrand.cinthya/projects "GitLab")

---

Actuellement en recherche d'emploi sur l'agglomération grenobloise.

Curieuse de tout, je souhaite découvrir de nouveaux domaines afin d'améliorer mes compétences.

J'espère pouvoir travailler avec vous très bientôt.
Bonne journée à vous. 

![Image](./static/cerveau.png){: style="width: 40%" }
