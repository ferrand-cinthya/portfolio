###Language de programation : 

-Python :

    - Data : 
        - Folium
        - Matplotlib 
        - Seaborn 
        - Panda
        - Beatifulsoup
        - Microstratégie
    
    - IA :
        - Pytorch
        - Keras
        - Pyspark
        - PIL 
        


-SQLite

-Postgresql

-Sas

-Teradata 

-Bach

###Les outils utilisers durant la formation : 

-Dockers

-Metabase

-Django

-Flask

-Power BI

- MicroStratégie


###Methodologie : 

-SCRUM
-AGILE
